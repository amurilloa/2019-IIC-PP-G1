# # carro:
# #     - marca
# #     - modelo
# #     - tamaño
# #     - nitro
# #     - cant_asientos
# #     - cilindraje
# #     - caballos
# #     - traccion
# #
# # celular:
# #     - num_serie
# #     - año frabricación
# #     - IMEI
# #     - modelo
# #     - color
# #     - tamaño
# #     - precio
# #     - marca
# #     - sistema operativo
# #     - capacidad almacenamiento
# #     - dueño *
# #
# # guitarra
# #     - marca
# #     - modelo
# #     - tipo
# #     - color
# #     - precio
# #     - estado
# #     - cant cuerdas
#
# #
class Persona:
    def __init__(self, ced=0, nom='', ape=''):
        self.cedula = ced
        self.nombre = nom
        self.apellido = ape

    def nombre_completo(self):
        return '{0} {1}'.format(self.nombre, self.apellido)

    def __str__(self):
        return '{0}-{1}'.format(self.cedula, self.nombre_completo())

p = Persona(1, 'Allan', 'Murillo')
# c = 206470762
# n = 'Allan'
# a = 'Murillo'
# p = Persona(c, n, a)
# p.nombre = 'Luis'
#
# print(p.nombre_completo())
p2 = Persona(2, 'Roberto', 'Murillo')
# p2.apellido = 'Soto'
# print(p2.nombre_completo())
#
# a = input('Apellido')
# p3 = Persona(12, 'Allan', a)
#
# print(p3.nombre_completo())
# print(type(p3))
#

class Perro:

    def __init__(self, nom):
        self.trucos = []
        self.nombre = nom

    def agregar_truco(self, truco):
        self.trucos.append(truco)

    def agregar_trucos(self, trucos):
        for x in trucos:
            self.trucos.append(x)

    def get_trucos(self):
        return self.trucos


p1 = Perro('Capitán')
# p2 = Perro('Chaca')

p1.agregar_truco('Hacerse el muerto')
p1.agregar_truco('Rodar')
# p2.agregar_truco('Rodar')

print(p1.nombre, p1.get_trucos())
# print(p2.nombre, p2.get_trucos())


dic = {}

dic[p.cedula] = p
dic[p2.cedula] = p2

print(dic)

for x in dic:
    print(dic[x])

# Clases
# Atributos (identificador algo que los haga únicos unos de otros, id=1)
# Inicializar los objetos(init/constructor)
# Métodos -->comportamiento

# Clase:Estructura vs Objeto/Instancia: Cuando uso la clase

