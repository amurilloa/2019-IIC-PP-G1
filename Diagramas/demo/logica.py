import datetime

# Variables Globales
dic = {}
codMascota = 0
razas = ['Pastor Alemán', 'Boxer', 'Pitbull', 'Husky', 'Otro']


def impMascotas():
    for x in dic:
        print('key:', x, 'valor:', dic[x])


def impRazas():
    txt = ''
    cont = 1  # Es para mostrar al usuario, no podemos empezarlo en 0
    for x in razas:
        txt += '{0}. {1}\n'.format(cont, x)
        cont += 1
    return txt


def buscarMas(filtro):
    txt = ''
    con = 1
    for x in dic:
        mascota = dic[x]
        if filtro in mascota:
            txt += '{0}. {1} - {2}\n'.format(con, mascota[0], mascota[1])
            con += 1
    if len(txt) == 0:
        txt = 'No hay mascotas que coincidan con la búsqueda.'
    return txt


def genCodMas():
    """
    :return:
    """
    global codMascota  # Para que poder usar la variable declarada en la parte superior

    fecha = datetime.datetime.now()
    codMascota += 1
    cod = fecha.year * 1000 + codMascota
    return cod


def regMascota(mascota):
    cod = mascota[0]  # El codigo de la mascota, campo 0 de la lista
    dic[cod] = mascota


def pregunta(preg):
    preg += '\nDigite SI u otro para NO: '
    while True:
        res = input(preg).lower()
        if len(res) > 0:
            res = res[0]
            if res == 's' or res == 'y':
                return True
            elif res == 'n':
                return False
        print('Favor digitar los solicitado. ')


def leerInt(mensaje, max=-1):
    """
    Para que sirve o el objetivo del método
    :param mensaje: que hace en el método el mensaje ???
    :param max: que hace el parámetro max ? para que sirve ??
    :return: Que retorna el método ?
    """
    while True:
        try:
            res = int(input(mensaje))
            if (res >= 1 and res <= max) or max == -1:
                return res
            else:
                print('\nOpción inválida, ', end='')
        except ValueError:
            print('\nFavor intente nuevamente, ', end='')


# Datos de Prueba
mascota = [genCodMas(), 'Capitán', 'Pastor Alemán', '07/30/2017', True, 'Allan Murillo']
regMascota(mascota)

mascota = [genCodMas(), 'Firulais', 'Pincher', '05/30/2016', False, 'Allan Murillo']
regMascota(mascota)

mascota = [genCodMas(), 'Pancho', 'Pastor Alemán', '10/10/2018', True, 'Gabriel']
regMascota(mascota)
