# from demo.logica import *
import demo.logica as log

# Menu Principal
# 1. Iniciar Sesión
#     - Solicitar: usuario y contraseña
#       NO: Vuelve al anterior
#       SI: Menu Secretaria o Médico(Revisar el TIPO)
# 2. Registrarme
# 3. Consulta de Cliente
# 4. Salir

#  Menu de Secretaria
# 1. -----
# 2. -----

# Mascota: id, nombre(str), raza(str), fecha nacimiento(str), pedigree(bool), dueño(str)
#  generar el ultimo + 1

menuP = ('\nMascotas - UTN v0.1\n'
         '1. Mascotas\n'
         '2. Salir\n'
         'Seleccione una opción: ')

menuM = ('\nMascotas - UTN v0.1\n'
         '1. Registrar Mascota\n'
         '2. Buscar Mascota\n'
         '3. Volver\n'
         'Seleccione una opción: ')

pregBus = ('Buscar por: \n'
           '1. Dueño\n'
           '2. Raza\n'
           '3. Cancelar\n'
           'Seleccione una opción: ')

while True:  # Menu principal
    opP = log.leerInt(menuP, 2)
    if opP == 1:  # Opción de Mascota
        while True:  # Menu de Mascota
            opM = log.leerInt(menuM, 3)
            if opM == 1:  # Registrar Mascota
                cod = log.genCodMas()
                nom = input('Nombre: ')
                print('Seleccione una raza:\n' + log.impRazas())
                pos = log.leerInt('Raza:', len(log.razas))
                raz = log.razas[pos - 1]
                fec = input('Fecha(dia/mes/año): ')
                ped = log.pregunta('¿Tiene Pedigree?')
                due = input('Dueño: ')
                masc = [cod, nom, raz, fec, ped, due]
                log.regMascota(masc)
            elif opM == 2:  # Buscar Mascota
                # log.impMascotas()
                p = log.leerInt(pregBus, 3)
                if p == 1:
                    nom = input('Nombre: ')
                    res = log.buscarMas(nom)
                    print(res)
                elif p == 2:
                    print('Seleccione una raza:\n' + log.impRazas())
                    pos = log.leerInt('Raza:', len(log.razas))
                    raz = log.razas[pos - 1]
                    print('Resultados para ' + raz)
                    res = log.buscarMas(raz)
                    print(res)
                # elif p == 3:
                #     pass
            elif opM == 3:  # Volver
                break
    elif opP == 2:  # Salir
        print('Gracias por utilizar la aplicación')
        break
