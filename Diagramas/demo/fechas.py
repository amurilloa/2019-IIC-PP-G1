import datetime as dt


def leerFecha(msj):
    while True:
        try:
            temp = input(msj)
            formato = '%d/%m/%Y'
            fecha = dt.datetime.strptime(temp, formato)
            return fecha
        except:
            print('Intente nuevamente, ej. 28/06/1998')


def impFecha(fecha):
    formato = '%d/%m/%Y'#'%A, %B %d, %Y'
    fechaStr = fecha.strftime(formato)
    return fechaStr


# fecha = leerFecha('Fecha Nacimiento: ')
# print(impFecha(fecha))

# print('Valor: %d' % 12, 'asd: %d' % 13)

# formato = '%a %d/%m/%Y %I%p'
# fechaNac1 = dt.date.strftime(fechaVal1,formato)
# fechaNac2 = dt.date.strftime(fechaVal2,formato)
#
# print(fechaNac1)
# print(fechaNac2)
# print(fechaNac1<fechaNac2)



f1 = leerFecha('Fecha 1: ')
# f2 = leerFecha('Fecha 2: ')
# print(f1>f2)
print(impFecha(f1))

