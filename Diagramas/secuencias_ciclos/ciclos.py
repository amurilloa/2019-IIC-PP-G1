# tabla = 5  # int(input('Tabla deseada: '))
# # for x in (1, 2, 3, 4, 5, 6, 7, 8, 9, 10):
# #     print('{0}x{1}={2}'.format(tabla, x, tabla*x))
#
# for x in range(11):
#     print('{0}x{1}={2}'.format(tabla, x, tabla * x))
#
# print('\nRango solo con 0,10')
# for x in range(0, 10):  # [0,10[
#     print(x, end=' ')
#
# print('\n\nRango solo con 10, 30, 2')
# for x in range(10, 30, 2):  # [10,30[ contando de 2 en 2
#     print(x, end=' ')
#
# print('\n\nRango solo con 10, 0, -1')
# for x in range(10, 0, -1):
#     print(x, end=' ')
#
# print('\n\nUsando el for con una lista definida(3,2,1)')
# for x in (3, 2, 1):
#     print(x, end=' ')
#
# print('\n\nUsando el for con una lista definida(3,2,1)')
# for x in (234, 34, 23, 54, 3, 2, 1):
#     print(x, end=' ')
#
# print('\n\nDe 1 a 10 con un while')
# num = 1
# while num <= 10:
#     print(num, end=' ')
#     num = num + 1
#
# print('\n\nDe 1 a 10 con un for')
# for x in range(1, 11):
#     print(x, end=' ')
#
# print('\n\nwhile en un menu')
# menu = ('Menu Prueba = UTN v0.1\n'
#         '1. Continuar\n'
#         '2. Salir')
# salir = False
# while not salir:
#     op = int(input(menu))
#     if op == 1:
#         print('Continuar...')
#     elif op == 2:
#         salir = True
#     else:
#         print('Opción Inválida!!')
#
# print('\n\nwhile en un menu')
# menu = ('Menu Prueba = UTN v0.2\n'
#         '1. Continuar\n'
#         '2. Salir')
#
# import os
#
# while True:
#     op = int(input(menu))
#     # os.system('cls')
#     # print('\n'*100)
#     if op == 1:
#         print('Continuar...\n\n')
#     elif op == 2:
#         break
#     else:
#         print('Opción Inválida!!')
#
# for x in range(1, 10):
#     print(x)
#
# while True:
#     op = int(input('1. xxxx, 2. salir'))
#     if op == 1:
#         pass
#     elif op == 2:
#         break  # romper el ciclo
#     else:
#         pass
#
# salir = False
# while not salir:
#     op = int(input('1. xxxx, 2. salir'))
#     if op == 1:
#         pass
#     elif op == 2:
#         salir = True
#     else:
#         pass

monto = 10000
while monto > 1:
    if monto%2==0:
        monto = monto // 2
    else:
        monto +=1
    print(monto)

for x in range(20): # [1,20[
    print(x, end=', ')

print()
tab = 5
for x in range(1,11):
    if x%2==0:
        end ='\n'
    else:
        end = '\t'
    print('{0}x{1}={2}'.format(tab, x, (tab*x)), end=end)

