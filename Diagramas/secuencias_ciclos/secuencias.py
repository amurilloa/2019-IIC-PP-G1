# Secuencias
txt = 'hola_mundo'
lista = [1, 4, 2, 4]
listaMixta = [12, 'ss', True, 12.2]
tupla = (1, 2, 3, 4, 1, 4, 5)

# Indexación positiva
# h|o|l|a| |m|u|n|d|o
# 0|1|2|3|4|5|6|7|8|9

# Indexación negativa
#   h| o| l| a|  | m| u| n| d| o
# -10|-9|-8|-7|-6|-5|-4|-3|-2|-1

# Primer elemento
a = txt[0]
print('Primer Elemto:', a)
# Último elemento
print('Último elemento:', txt[-1])

# Mebresía
print('Existe la O en hola mundo:', 'O' in txt)
print('Existe la o en hola mundo:', 'o' in txt)
print('Existe la 12.2 en Lista Mixta:', 12.2 in listaMixta)

# Como recorrer una secuencia
for x in txt:
    print(x)

# Segmento
print(txt[2:8])  # [2,8[
# Segmento de un número hasta el final
print(txt[2:])  # [2:final]
# Segmento desde el inicio hasta el n(no se incluye)
print(txt[:6])  # [incio:6[

# min, max, len
print(min(txt))
print(max(txt))
largo = len(txt)
print('Largo:', largo)
print('Último elemento con indexación positiva:', txt[largo - 1])
print('-->', len(listaMixta))
# range
x = list(range(100000))
# print(x)

print(txt)
txt = txt.capitalize()
print(txt.capitalize())
print(txt)

temp = 'H'
for x in txt[1:]:  # 'ola_mundo'
    if x == '_':
        temp += ' '
    else:
        temp += x
txt = temp
print(txt)

# Métodos para trabajar con Strings
txt = 'casa roja'
# capitalize
print(txt.capitalize())  # Genera un string con la primera letra en mayúscula, no lo modifica
# Si lo quiero modificar debo asignarlo a la misma variable
txt = txt.capitalize()
print(txt)
# count
print('Cantidad a:', txt.count('a'))  # Cantidad de apariciones de a en el texto
print(txt.endswith('ja'))  # Si termina con, retorna true o false
print(txt.startswith('Ca'))  # Si a palabra empieza con
# find y index : Nos permiten buscar el indice de una letra en un string
print(txt.find('a'))
print(txt.find('a', 2))
print(txt.find('a', 4))
print(txt.find('a', 9))

print(txt.index('a'))
print(txt.index('a', 2))
print(txt.index('a', 4))
if 'z' in txt:
    print(txt.index('z'))
else:
    print('no hay z')

placa = 'BHX 692'
print(placa.isalnum())  # True cuando en el contenido solo hay letras y números
print('casa roja'.isalpha())

print('123'.isdecimal())  # Si todos los elementos son decimal
print('12'.isdigit())  # si todos los elementos son digitos  ---> preferido
print('123'.isnumeric())  # si todos los elementos son numéricos

print('2²', '2²'.isdecimal())
x = '2' + '\u00B2'
print(x, x.isdecimal())

print('casa está en minúscula? :', 'casa'.islower())
print('CASA está en mayúscula? :', 'CASA'.isupper())

# txt = ''
# for x in range(5):
#     username = input('Digite su nombre de usuario: ')
#     txt += username.lower()[0:8].capitalize() + '\n'
# print(txt)


# txt = ''
# for x in range(5):
#     username = input('Digite su nombre de usuario: ')
#     txt += username.upper() + '\n'
# print(txt)

# amurillo
# lperez
# JMora
# rMatamoros.lower() --> rmatamoros[0:8]--> rmatamor
# cCaStRo

lista = ['texto', 'casa', 'roja', 'hola', 'mundo']
delim = ','
nueva = delim.join(lista)
print(nueva)
print(delim)
print(nueva.split(','))


def esPar(num):
    if num % 2 == 0:
        return (True, '{0} es par'.format(num))
    else:
        return (False, '{0} no es par'.format(num))


x, y = esPar(6)
print(x)
print(y)

res = esPar(7)
print(res[0])
print(res[1])


# Pedir una frase y retornarala alrevés
def ejercicio1v1(frase):
    return frase[::-1]


def ejercicio1v2(frase):
    nueva = ''
    for letra in frase:
        nueva = letra + nueva  #
    return nueva


def ejercicio1v3(frase):
    nueva = ''
    con = -1
    while con >= -len(frase):
        nueva += frase[con]
        con -= 1
    return nueva
    # hola + mundo
    # mundo + hola


# print(ejercicio1v3('hola'))

# Pedir una frase y eliminar los espacios en blanco, mostrar
#       Frase original
#       Frase resultante
#       Cantidad de Espacios Eliminados

def ejercicio3(frase):
    nueva = ''
    esp = 0
    for let in frase:
        if let == ' ':
            esp += 1
        else:
            nueva += let
    return (frase, nueva, esp)


def ejercicio3v2(frase):
    lista = frase.split(' ')
    nueva = ''.join(lista)
    return (frase, nueva, len(frase) - len(nueva))


print(ejercicio3v2('ho la mun do'))


# de un caracter cualquiera si es consonante mayúscula o minúscula, o si es vocal mayúscula
# o minúscula.

# 'a' --> vocal minúscula
# 'E' --> vocal mayúscula
# 'S' --> consonante mayúscula
# 'x' --> consonante minúscula
# '+' --> otro caracter

def ejercicio9(letra):
    res = ''
    if letra.isalpha():
        if letra.islower():
            res += 'minúscula'
        else:
            res += 'mayúscula'

        if letra.lower() in 'aeiou':
            res += ' vocal'
        else:
            res += ' consonante'
    else:
        res = 'es otro caracter'
    return res

print('a', ejercicio9('a'))
print('E', ejercicio9('E'))
print('S', ejercicio9('S'))
print('x', ejercicio9('x'))
print('+', ejercicio9('+'))

# cantidad de dígitos en una frase
# "un 1  y un 20" --> 3 dígitos
def ejercicio19(frase):
    con = 0
    for x in frase:
        if x.isdigit():
            con += 1
    return con

print(ejercicio19('un 1  y un 20'), 'dígitos')
