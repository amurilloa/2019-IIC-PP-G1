# Pagina 91
def ejercicio01():
    while True:
        num = int(input('Digite un número(0-10): '))
        if num >= 0 and num <= 10:
            print('Correcto!!')
            break
        else:
            print('Favor intente nuevamente')


def ejercicio02():
    menor = 'SN'
    mayor = -1
    numeros = ''
    while True:
        num = int(input('Digite un número: '))
        if num < 0:
            res = ('Números: {0}\n'
                   'Mayor: {1}\n'
                   'Menor: {2}').format(numeros, mayor, menor)
            return res
        numeros += '{0} '.format(num)
        if menor == 'SN' or num < menor:
            menor = num
        if mayor == -1 or num > mayor:
            mayor = num


def ejercicio03():
    numeros = ''
    for x in range(6, 151):
        if x % 6 == 0:
            numeros += '{0} '.format(x)
    # for x in range(6, 151, 6):
    #     print(x, end=' ')
    return numeros


def ejercicio04(n, m):
    pass


def ejercicio05(n, m):
    potencia = 0
    resp = ''
    while True:
        res = 2 ** potencia
        if res >= n and res <= m:
            resp += '2^{0}={1}\n'.format(potencia, res)
        elif res > m:
            break
        potencia += 1
    return resp


def ejercicio06(n, m):
    total = 0
    resp = ''
    for x in range(n, m + 1):  # (1,2,3,4,5,6,7,8,9,10)
        total += x
        resp += '{0}+'.format(x)  # '1+2+3+4+5+6+7+8+9+10+'
    resp += '\b'
    return '{0}={1}'.format(resp, total)


# print(ejercicio06(1, 10))

def ejercicio07(n, m):
    if n > m:
        return 'El 1er número debe ser mayor o igual que el 2ndo'

    total = 0
    resp = ''
    for x in range(n, m + 1):  # (1,2,3,4,5,6,7,8,9,10)
        total += x
        resp += '{0}+'.format(x)  # '1+2+3+4+5+6+7+8+9+10+'
    resp += '\b'
    return '{0}={1}'.format(resp, total)


# print(ejercicio07(18, 16))

def ejercicio08():
    resp = ''
    for n in range(1, 201):
        if n % 2 == 0:
            if n > 0 and n % 30 == 0:
                nl = '\n'
            else:
                nl = ''
            resp += '{0:4} {1}'.format(n, nl)  # str(n) + ', '

    resp += '\b'
    return resp


# print(ejercicio08())

def ejercicio09():
    resp = ''
    cont = 0
    for num in range(200, -1, -1):
        if num % 2 == 0:
            if cont >= 10:
                end = '\n'
                cont = 0
            else:
                end = ''
            cont += 1
            resp += '{0:4} {1}'.format(num, end)
    return resp + '\b\b'


# print(ejercicio09())

def ejercicio10(n):
    res = ''
    for x in range(n + 1):
        if x % 2 == 0:
            res += '{0} '.format(x)
    return res


# print(ejercicio10(40))

def ejercicio11(n, m):
    res = ''
    tot = 0
    if n <= m:
        for i in range(n, m + 1):
            tot += i ** 2
            res += '{0}²+'.format(i)
    else:
        for i in range(n, m - 1, -1):
            tot += i ** 2
            res += '{0}²+'.format(i)

    res += '\b'
    return '{0}={1}'.format(res, tot)


# print(ejercicio11(1, 5))
# print(ejercicio11(5, 1))

def ejercicio12(n, m):
    res = ''
    tot = 0
    for x in range(n, m + 1):
        if x % 2 == 0:
            tot += x
            res += '{0}+'.format(x)
    res += '\b'
    return '{0}={1}'.format(res, tot)


# print(ejercicio12(1,10))

def ejercicio13():
    res = ''
    for x in range(1, 101):
        fin = '\n' if x % 10 == 0 else ''
        res += '{0:3} {1}'.format(x, fin)
    return '{0}\nFin del programa'.format(res)


# print(ejercicio13())

def ejercicio14():
    # n1+n2+n3+...n = tot/cantNotas
    print('Si desea terminar digite un número negativo!')
    preg = ('Nota Estudiante #{0}: ')
    cont = 0
    total = 0
    while True:
        nota = int(input(preg.format(cont + 1)))
        if nota < 0:
            break
        total += nota
        cont += 1
    pro = total / cont
    return ('Promedio de Notas: {0:.2f}\n'
            'Cant. Estudiantes: {1}').format(pro, cont)


# print(ejercicio14())

def ejercicio15(x, y):
    total = 1
    res = ''
    for i in range(y):
        total *= x
        res += '{0}*'.format(x)
    res += '\b'
    return '{0}={1:.3f}'.format(res, total)


def esPrimo(num):
    div = 0
    for x in range(1, num + 1):
        if num % x == 0:
            div += 1
    return div <= 2


def ejercicio16():
    res = ''
    for x in range(1, 1):
        if esPrimo(x):
            res += '{0} '.format(x)
    return res


print(ejercicio16())  # Pagina 91


def ejercicio01():
    while True:
        num = int(input('Digite un número(0-10): '))
        if num >= 0 and num <= 10:
            print('Correcto!!')
            break
        else:
            print('Favor intente nuevamente')


def ejercicio02():
    menor = 'SN'
    mayor = -1
    numeros = ''
    while True:
        num = int(input('Digite un número: '))
        if num < 0:
            res = ('Números: {0}\n'
                   'Mayor: {1}\n'
                   'Menor: {2}').format(numeros, mayor, menor)
            return res
        numeros += '{0} '.format(num)
        if menor == 'SN' or num < menor:
            menor = num
        if mayor == -1 or num > mayor:
            mayor = num


def ejercicio03():
    numeros = ''
    for x in range(6, 151):
        if x % 6 == 0:
            numeros += '{0} '.format(x)
    # for x in range(6, 151, 6):
    #     print(x, end=' ')
    return numeros


def ejercicio04(n, m):
    pass


def ejercicio05(n, m):
    potencia = 0
    resp = ''
    while True:
        res = 2 ** potencia
        if res >= n and res <= m:
            resp += '2^{0}={1}\n'.format(potencia, res)
        elif res > m:
            break
        potencia += 1
    return resp


def ejercicio06(n, m):
    total = 0
    resp = ''
    for x in range(n, m + 1):  # (1,2,3,4,5,6,7,8,9,10)
        total += x
        resp += '{0}+'.format(x)  # '1+2+3+4+5+6+7+8+9+10+'
    resp += '\b'
    return '{0}={1}'.format(resp, total)


# print(ejercicio06(1, 10))

def ejercicio07(n, m):
    if n > m:
        return 'El 1er número debe ser mayor o igual que el 2ndo'

    total = 0
    resp = ''
    for x in range(n, m + 1):  # (1,2,3,4,5,6,7,8,9,10)
        total += x
        resp += '{0}+'.format(x)  # '1+2+3+4+5+6+7+8+9+10+'
    resp += '\b'
    return '{0}={1}'.format(resp, total)


# print(ejercicio07(18, 16))

def ejercicio08():
    resp = ''
    for n in range(1, 201):
        if n % 2 == 0:
            if n > 0 and n % 30 == 0:
                nl = '\n'
            else:
                nl = ''
            resp += '{0:4} {1}'.format(n, nl)  # str(n) + ', '

    resp += '\b'
    return resp


# print(ejercicio08())

def ejercicio09():
    resp = ''
    cont = 0
    for num in range(200, -1, -1):
        if num % 2 == 0:
            if cont >= 10:
                end = '\n'
                cont = 0
            else:
                end = ''
            cont += 1
            resp += '{0:4} {1}'.format(num, end)
    return resp + '\b\b'


# print(ejercicio09())

def ejercicio10(n):
    res = ''
    for x in range(n + 1):
        if x % 2 == 0:
            res += '{0} '.format(x)
    return res


# print(ejercicio10(40))

def ejercicio11(n, m):
    res = ''
    tot = 0
    if n <= m:
        for i in range(n, m + 1):
            tot += i ** 2
            res += '{0}²+'.format(i)
    else:
        for i in range(n, m - 1, -1):
            tot += i ** 2
            res += '{0}²+'.format(i)

    res += '\b'
    return '{0}={1}'.format(res, tot)


# print(ejercicio11(1, 5))
# print(ejercicio11(5, 1))

def ejercicio12(n, m):
    res = ''
    tot = 0
    for x in range(n, m + 1):
        if x % 2 == 0:
            tot += x
            res += '{0}+'.format(x)
    res += '\b'
    return '{0}={1}'.format(res, tot)


# print(ejercicio12(1,10))

def ejercicio13():
    res = ''
    for x in range(1, 101):
        fin = '\n' if x % 10 == 0 else ''
        res += '{0:3} {1}'.format(x, fin)
    return '{0}\nFin del programa'.format(res)


# print(ejercicio13())

def ejercicio14():
    # n1+n2+n3+...n = tot/cantNotas
    print('Si desea terminar digite un número negativo!')
    preg = ('Nota Estudiante #{0}: ')
    cont = 0
    total = 0
    while True:
        nota = int(input(preg.format(cont + 1)))
        if nota < 0:
            break
        total += nota
        cont += 1
    pro = total / cont
    return ('Promedio de Notas: {0:.2f}\n'
            'Cant. Estudiantes: {1}').format(pro, cont)


# print(ejercicio14())

def ejercicio15(x, y):
    total = 1
    res = ''
    for i in range(y):
        total *= x
        res += '{0}*'.format(x)
    res += '\b'
    return '{0}={1:.3f}'.format(res, total)


def esPrimo(num):
    div = 0
    for x in range(1, num + 1):
        if num % x == 0:
            div += 1
        if div > 2:
            return False
    return div <= 2


def ejercicio16():
    res = ''
    for x in range(1, 50000):
        if esPrimo(x):
            res += '{0} '.format(x)
    return res


# print(ejercicio16())

def esTripleta(c1, c2, h):
    return c1 ** 2 + c2 ** 2 == h ** 2


def ejercicio17():
    res = ''
    for h in range(1, 50):
        for c1 in range(1, 50):
            for c2 in range(1, 50):
                if esTripleta(c1, c2, h):
                    res += '{0}²+{1}²={2}²\n'.format(c1, c2, h)
    return res


# print(ejercicio17())


def ejercicio18v1(n1, n2):
    lim = min(n1, n2)
    for div in range(lim, 0, -1):
        if n1 % div == 0 and n2 % div == 0:
            return div
    return 1


def ejercicio18v2(n1, n2):
    lim = min(n1, n2)
    while lim > 0:
        if n1 % lim == 0 and n2 % lim == 0:
            return lim
        lim -= 1
    return 1


# res = ejercicio18v2(42, 60)  # ejercicio18v1(42, 60)
# print('MCD de {0} y {1} es {2}'.format(42, 60, res))


def ejercicio19(n1, n2, n3):
    lim = min(n1, n2, n3)
    for div in range(lim, 0, -1):
        if n1 % div == 0 and n2 % div == 0 and n3 % div == 0:
            return div
    return 1


res = ejercicio19(44, 60, 80)
print('MCD de {0},{1} y {2} es {3}'.format(44, 60, 80, res))


def ejercicio20(num):
    cant = 0
    for div in range(1, num + 1):
        if num % div == 0:
            cant += 1
        if div > 2:
            return False
    return cant <= 2


res = '' if ejercicio20(5) else 'no'
print('El número {0} {1} es primo'.format(5, res))

res = '' if ejercicio20(6) else 'no'
print('El número {0} {1} es primo'.format(6, res))


def ejercicio20v2(num):
    cont = 1
    div = 0
    while cont <= num:
        if num % cont == 0:
            div += 1
        if div > 2:
            return False
        cont += 1
    return div <= 2


# res = '' if ejercicio20v2(12) else 'no'
# print('El número {0} {1} es primo'.format(12, res))

# ejercicio23
def encriptar(num):
    back = num
    sum = 0
    while back > 0:
        sum = sum * 10 + 7
        back //= 10

    nuevo = num + sum
    resp = nuevo

    d1 = nuevo % 10
    nuevo = nuevo // 10
    d2 = nuevo % 10
    nuevo = nuevo // 10
    d3 = nuevo % 10
    nuevo = nuevo // 10
    d4 = nuevo % 10
    nuevo = nuevo // 10
    d5 = nuevo % 10
    nuevo = nuevo // 10
    d6 = nuevo % 10
    nuevo = nuevo // 10

    if d6 > 0:
        num2 = ((((d1 * 10 + d3) * 10 + d4) * 10 + d2) * 10 + d5) * 10 + d6
    elif d5 > 0:
        num2 = ((((d1 * 10 + d2) * 10 + d3) * 10 + d4) * 10 + d5)
    elif d4 > 0:
        num2 = (((d1 * 10 + d3) * 10 + d2) * 10 + d4)
    elif d3 > 0:
        num2 = ((d1 * 10 + d2) * 10 + d3)
    elif d2 > 0:
        num2 = (d1 * 10 + d2)
    else:
        num2 = d1
    print(num2)


# encriptar(1254)



