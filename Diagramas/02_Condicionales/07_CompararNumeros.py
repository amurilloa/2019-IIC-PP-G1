# Determinar sin de dos numeros cual es el mayor o si son iguales

# Entradas
numUno = int(input('#1: '))
numDos = int(input('#2: '))

# Proceso y Salidas
if numUno > numDos:
    print('>>', numUno, 'es el mayor')
elif numDos > numUno:
    print('>>', numDos, 'es el mayor')
else:
    print('>>', 'Son iguales')
