# Determinar si una persona es mayor de edad, para ser mayor
# de edad debe tener mayor o igual a 18 años

#Entradas
edad = int(input('Digite su edad: '))

#Proceso y Salida
if edad >= 18:
    print('Es mayor de edad')
else:
    print('Es menor de edad')

