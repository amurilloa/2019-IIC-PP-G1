# sumar dos numeros enteros
# def + nombre + (parametros) + :
# EVITAR: Pedir datos dentro de una función o imprimir datos dentro de la función
def sumar(n1, n2):
    """ suma dos números enteros, return n1 + n2, sumar(n1, n2)"""
    sum = n1 + n2
    return sum


def saludar(nom):
    """Saluda una persona, return Hola, + nom """
    saludo = 'Hola, ' + nom
    return saludo
    # return 'Hola, ' + nom


def areaTriangulo(b, h):
    """ Calcula el área de un triángulo, return area"""
    a = b * h / 2
    return a


# Menu - Usuario
menu = ('Menu - UTN v0.1\n'
        '1. Sumar\n'
        '2. Saludar\n'
        '3. Salir\n'
        'Seleccione una opción: ')

op = int(input(menu))
if op == 1:
    # Entradas
    a = int(input('#1: '))
    b = int(input('#2: '))

    res = sumar(a, b)

    # Salidas
    print(a, '+', b, '=', res)
    print('El resultado de la suma es:', res)
elif op == 2:
    n = input('Digite su nombre: ')
    print(saludar(n))
elif op == 3:
    print('Gracias por utilizar la aplicación')
else:
    print('Opción inválida')
