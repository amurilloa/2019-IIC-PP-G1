import math

# Determinar la hipotenusa de un triangulo rectangulo

# Entradas
catUno = float(input('Medida Cateto #1: '))
catDos = float(input('Medida Cateto #2: '))

# Proceso
sum = math.pow(catUno, 2) + catDos ** 2
hip = math.sqrt(sum)

# Salida
print('Hipotenusa:', hip)
