# Calcular el precio medio de un articulo, a partir del precio de
# en tres establecimientos distintos

# Entradas
pUno = int(input('Precio #1: '))
pDos = int(input('Precio #2: '))
pTres = int(input('Precio #3: '))

# Proceso
pMedio = (pUno+pDos+pTres)/3

# Salida
print('El precio medio es: ', pMedio, 'CRC')