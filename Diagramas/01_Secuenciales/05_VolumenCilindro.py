import math

# Calcular el volumen de un cilindro dados el diametro y la altura
# Entradas
print('Volumen del Cilindro')
dia = int(input('Diametro: '))
alt = int(input('Altura: '))

# Proceso
rad = dia / 2
vol = math.pi * rad ** 2 * alt

# Salida
print('Volumen del cilidro:', vol)
