# Para imprimir usar el estatuto print()
# print('hola mundo')
# print(1222.233)
# print(100,000,000)
# print("hola",233,"mundo")

# Sumar dos números enteros
n1 = 23
n2 = 45
n3 = n1 + n2 # + - * / %
print(n1,"+",n2,"=",n3)
print("Resultado:",n3)
print(n3) #No hacer esto, imprimir algo que entienda el usuario

# Saludar, pedir nombre: Hola, nombre
# nombre = input('Digite su nombre: ')
# print("Hola,", nombre)

# casting str --> int
# type me dice el tipo de una variable
# int(x), float(x), bool(x), str(x) --> convertir al tipo solicitado
txt = '5'
print(type(txt))
num1 = int(txt)
print(type(num1))
num2 = float(txt)
print(num1)
print(num2)
print(txt)
# x = int('23')
# x = int(txt)
# x = int(input('Digite un número'))

