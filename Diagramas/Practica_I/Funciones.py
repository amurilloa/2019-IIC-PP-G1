import random
import math
import sys


def ejercicio1(cant):
    """
    Calcula el costo con descuento por la compra de N llantas
    :param cant: Cantida de Llantas a comprar
    :return: Precio total a cancelar con descuento incluido
    """
    if cant < 5:
        total = cant * 80
    else:
        total = cant * 70
    return total


def ejercicio1v2(cant):
    """
    Calcula el costo con descuento por la compra de N llantas
    :param cant: Cantida de Llantas a comprar
    :return: Precio total a cancelar con descuento incluido
    """
    if cant < 5:
        return cant * 80
    else:
        return cant * 70

        # 10000
    # 0 - 50-->15%     50-100-->20%


def ejercicio2(compra):
    ale = random.randint(1, 100)
    # print(ale) desarrollo.

    if ale < 50:
        return compra * 0.15
    else:
        return compra * 0.20


def ejercicio3(x):
    if x % 4 == 0:
        return x * x
    elif x % 4 == 1:
        return x / 6
    elif x % 4 == 2:
        return math.sqrt(x)
    elif x % 4 == 3:
        return x * x * x + 5


def esPar(num):
    return num % 2 == 0


def ejercicio5(num):
    eval = num // 2  # 7
    return not esPar(eval) and eval * 2 == num


def ejercicio6(n1, n2):
    cn1 = n1 ** 2
    if n2 == cn1:
        return 'El segundo es el cuadrado exacto del primero'
    elif n2 < cn1:
        return 'El segundo es menor que el cuadrado del primero'
    else:
        return 'El segundo es mayor que el cuadrado del primero'


def ejercicio7(n1, n2, n3, n4, n5):
    if type(n1) == int:
        men = n1
        may = n1
    else:
        men = sys.maxsize
        may = -sys.maxsize - 1
    msj = ''
    if type(n2) == int:
        if n2 < men:
            men = n2
        if n2 > may:
            may = n2
    else:
        msj += str(n2) + ' no es entero\n'

    if type(n3) == int:
        if n3 < men:
            men = n3
        if n3 > may:
            may = n3
    else:
        msj += str(n3) + ' no es entero\n'

    if type(n4) == int:
        if n4 < men:
            men = n4
        if n4 > may:
            may = n4
    else:
        msj += str(n4) + ' no es entero\n'

    if type(n5) == int:
        if n5 < men:
            men = n5
        if n5 > may:
            may = n5
    else:
        msj += str(n5) + ' no es entero\n'

    msj += 'El número menor es ' + str(men) + ', el mayor es ' + str(may)
    return msj


def ejercicio8(n1, n2, n3, n4, n5):
    may = n1
    if n2 > may:
        may = n2
    if n3 > may:
        may = n3
    if n4 > may:
        may = n4
    if n5 > may:
        may = n5
    return may


def ejercicioMax8v2(n1, n2, n3, n4, n5):
    return max(n1, n2, n3, n4, n5)


def ejercicioMin8v2(n1, n2, n3, n4, n5):
    return min(n1, n2, n3, n4, n5)


def ejercicio9(n1, n2, n3, n4, n5):
    difMen = abs(n1 - n2)
    men = n2

    dif = abs(n1 - n3)
    if dif < difMen:
        difMen = dif
        men = n3

    dif = abs(n1 - n4)
    if dif < difMen:
        difMen = dif
        men = n4

    dif = abs(n1 - n5)
    if dif < difMen:
        difMen = dif
        men = n5
    return men


def ejercicio10(hect):
    MTS_X_HEC = 10000
    mts = hect * MTS_X_HEC

    if mts <= 1000000:
        mtsPino = mts * 0.45
        mtsEuca = mts * 0.30
        mtsCedr = mts * 0.25
    else:
        mtsPino = mts * 0.70
        mtsEuca = mts * 0.20
        mtsCedr = mts * 0.10

    canPino = math.trunc(8 * mtsPino / 10)
    canEuca = math.trunc(15 * mtsEuca / 15)
    canCedr = math.trunc(10 * mtsCedr / 18)

    res = ('- {0:8} Pinos\n'
           '- {1:8} Eucaliptos\n'
           '- {2:8} Cedros'.format(canPino, canEuca, canCedr))
    return res


def ejercicio11(valor, dev, inc):
    aum = valor * (inc / 100) * 3
    dec = valor * (dev / 100) * 3

    if dec > aum / 2:
        return 'No comprar el automóvil'
    else:
        return 'Puede comprar el automóvil'


def ejercicio12(x1, y1, x2, y2, x3, y3, x4, y4, x5, y5):
    masCercanoX = x2
    masCercanoY = y2
    distCer = math.sqrt(math.pow((x1 - masCercanoX), 2) + math.pow((y1 - masCercanoY), 2))
    dist = math.sqrt(math.pow((x1 - x3), 2) + math.pow((y1 - y3), 2))
    if dist < distCer:
        masCercanoX = x3
        masCercanoY = y3
        distCer = dist

    dist = math.sqrt(math.pow((x1 - x4), 2) + math.pow((y1 - y4), 2))

    if dist < distCer:
        masCercanoX = x4
        masCercanoY = y4
        distCer = dist

    dist = math.sqrt(math.pow((x1 - x5), 2) + math.pow((y1 - y5), 2))
    if dist < distCer:
        masCercanoX = x5
        masCercanoY = y5
        distCer = dist

    return ('El punto en las coordenadas ({0},{1})\n'
            'es el mas cercano al punto ({2},{3})\n'
            'con una distancia de {4:.2f}'.format(masCercanoX, masCercanoY, x1, y1, distCer))


def ejercicio13(actual, monto, lim, imp):
    int = actual * (imp / 100)
    nuevo = actual + monto + int
    if nuevo > lim:
        return 'Transacción no puede ser realizada'
    else:
        return 'Transacción realizada con éxito'


def ejercicio14(sexo, edad):
    if sexo == 'F':
        return (220 - edad) / 10
    else:
        return (210 - edad) / 10


def ejercicio15(monto):
    if monto < 500:
        return monto * 0.03
    else:
        return monto * 0.02


def ejercicio16(cant, prom, costInt):
    COS_X_MAT = 100
    if prom >= 70:
        costoTemp = cant * COS_X_MAT
        total = costoTemp * 0.70
        txt = '{0}x{1}'.format(cant, COS_X_MAT)
        fact = ('Materias:{0:^10}= ${1}\n'
                '    Desc:   30%    = ${2}\n'
                'Internet:   Free   = $0\n'
                '   Total:          = {3}')
        return fact.format(txt, costoTemp, costoTemp * 0.30, total)
    else:
        costoTemp = cant * COS_X_MAT
        total = costoTemp + costInt
        txt = '{0}x{1}'.format(cant, COS_X_MAT)
        fact = ('Materias:{0:^10}= ${1}\n'
                'Internet:          = ${2}\n'
                '   Total:          = {3}')
        return fact.format(txt, costoTemp, costInt, total)


def ejercicio17(cap, int, anos):
    if int > 0:
        return cap * (1 + int / 100) * anos
    return 'El interés no puede negativo'


def ejercicio18(nota):
    if nota >= 10:
        return 'Matricula de Honor'
    elif nota >= 8.5:
        return 'Sobresaliente'
    elif nota >= 7:
        return 'Notable'
    elif nota >= 5:
        return 'Suspenso'
    else:
        return 'Reprobado'


def ejercicio19(monto):
    detalle = ''
    
    dem = 500
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle = detalle + str(can) + ' billetes de ' + str(dem) + '\n'

    dem = 200
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle = detalle + str(can) + ' billetes de ' + str(dem)+ '\n'

    dem = 100
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle = detalle + str(can) + ' billetes de ' + str(dem)+ '\n'

    dem = 50
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle = detalle + str(can) + ' billetes de ' + str(dem)+ '\n'

    dem = 20
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle = detalle + str(can) + ' billetes de ' + str(dem)+ '\n'

    dem = 10
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle = detalle + str(can) + ' billetes de ' + str(dem)+ '\n'

    dem = 5
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle = detalle + str(can) + ' billetes de ' + str(dem)+ '\n'

    dem = 2
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle = detalle + str(can) + ' monedas de ' + str(dem)+ '\n'

    if monto > 0:
        detalle = detalle + str(monto) + ' monedas de 1'

    return detalle


