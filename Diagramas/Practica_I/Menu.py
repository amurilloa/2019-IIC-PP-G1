# #Importando otras librerías
# # Más utilizados y recomendados (opcionalmente puede llevar un alias as nombre )
# import math
#
# # El recomendado
# import Practica_I.Funciones as func
#
# # Cuando la librería es muy grande y solo necesito 1 método o constante
# from  math import pi, sqrt # no utilizar funciones o variables con los mismos

# Darle formato a los textos: https://pyformat.info/

import Practica_I.Funciones as func

print('>> Ejercicio #1: Llantas <<')
print('Comprando 5 llantas:', func.ejercicio1(5))
print('Comprando 4 llantas:', func.ejercicio1(4))

print('>> Ejercicio #2: Descuento <<')
print('Compra 10000:', func.ejercicio2(10000))
print('Compra 10000:', func.ejercicio2(10000))

print('>> Ejercicio #3: Funciones <<')
print('f(1):', func.ejercicio3(1))
print('f(2):', func.ejercicio3(2))
print('f(3):', func.ejercicio3(3))
print('f(4):', func.ejercicio3(4))

print('>> Ejercicio #4 Es par <<')
msj = 'par' if func.esPar(4) else 'impar'
print('Él número 4, es ', msj)
print('Él número 5, es ', 'par' if func.esPar(5) else 'impar')

print('>> Ejercicio #5 Doble de un impar <<')
print('Él número 6,', 'es el doble de un impar' if func.ejercicio5(6) else 'no es el doble de un impar')
print('Él número 14,', 'es el doble de un impar' if func.ejercicio5(14) else 'o es el doble de un impar')

print('>> Ejercicio #6 Cuadrados <<')
print('N1: 5, N2: 25 -->> ', func.ejercicio6(5, 25))
print('N1: 25, N2: 5 -->> ', func.ejercicio6(25, 5))
print('N1: 1, N2: 2 -->> ', func.ejercicio6(1, 2))

print('>> Ejercicio #7 Mayor y Menor<<')
print('1, -3, 5, 3, \'0\'\n', func.ejercicio7(1, -3, 5, 3, '0'))

# # listas, tuplas, diccionarios
# print(max(1, -3, 5, 3, 0))
# print(min(1, -3, 5, 3, 0))

print('>> Ejercicio #8 Mayor <<')
print('1, -3, 5, 3, 0\nEl número mayor es:', func.ejercicio8(1, -3, 5, 3, 0))
print('1, -3, 5, 3, 0\nEl número mayor es:', func.ejercicioMax8v2(1, -3, 5, 3, 0))

print('>> Ejercicio #9 Número más cercano <<')
print('2, 6, 4, 1, 10 el número más cercano es:', func.ejercicio9(2, 6, 4, 1, 10))

print('>> Ejercicio #10 Reforestación <<')
print('En 101 hectáreas debería sembrar:\n{0}'.format(func.ejercicio10(101)))
print('En 80 hectáreas debería sembrar:\n{0}'.format(func.ejercicio10(80)))

print('>> Ejercicio #11 Compra de Vehiculos <<')
print(func.ejercicio11(10000000, 3, 2));

print('>> Ejercicio #12 Puntos Cercanos <<')
print(func.ejercicio12(1, 1, 2, 2, 3, 2, 1, 0, -2, -1))

print('>> Ejercicio #13 Tarjeta de Crédito <<')
print('Balance Actual: 470000\n'
      '        Compra: 20000\n'
      'Límite Crédito: 500000\n', func.ejercicio13(470000, 20000, 500000, 4))

print('>> Ejercicio #14 Pulsaciones <<')

print('Hombre de 15 años, ', func.ejercicio14('M', 15))
print('Mujer de 15 años, ', func.ejercicio14('F', 15))

print('>> Ejercicio #15 INS <<')

print('Monto $600, ', func.ejercicio15(600))
print('Monto $300, ', func.ejercicio15(300))

print('>> Ejercicio #15 INS <<')

print('Monto $600, ', func.ejercicio15(600))
print('Monto $300, ', func.ejercicio15(300))

print('>> Ejercicio #16 Universidad <<')

print('4 Materias, promedio de 71\n', func.ejercicio16(4, 71, 20))
print('4 Materias, promedio de 67\n', func.ejercicio16(4, 67, 20))

print('>> Ejercicio #17 Capital <<')
print('Un capital de $10000 al 2% por 3 años genera: ${0}'.format(func.ejercicio17(10000, 2, 3)))

print('>> Ejercicio #18 Notas Cualitativas<<')
print('Estudiante con nota de 4:', func.ejercicio18(4))
print('Estudiante con nota de 5:', func.ejercicio18(5))
print('Estudiante con nota de 7:', func.ejercicio18(7))
print('Estudiante con nota de 9:', func.ejercicio18(9))
print('Estudiante con nota de 10:', func.ejercicio18(10))


print('>> Ejercicio #19 Cajero Automático<<')
print(func.ejercicio19(888))

