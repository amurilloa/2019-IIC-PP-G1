a = int(input('Años laborados: '))
sal = int(input('Salario actual: '))
salAnu = sal * 12

if a < 4:
    aum = salAnu * 0.09
elif a < 6:
    aum = salAnu * 0.12
elif a < 11:
    aum = salAnu * 0.14
else:
    aum = salAnu * 0.16

aumM = aum / 12
salN = sal + aumM
print(' Actual: ', sal)
print('Aumento:  ', aumM)
print('  Total: ', salN)
