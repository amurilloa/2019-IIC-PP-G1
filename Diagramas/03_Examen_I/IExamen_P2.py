import math

op = int(input("MENU I EXAMEN - UTN v0.1\n"
               "1. Rectángulo\n"
               "2. Círculo\n"
               "3. Triángulo\n"
               "Seleccione una opción: "))

if op == 1:
    print('*** Rectángulo *** ')
    b = int(input('Base: '))
    h = int(input('Altura: '))
    area = b * h
    print('Área: ', area)
elif op == 2:
    print('*** Círculo *** ')
    d = int(input('Diametro: '))
    r = d / 2
    area = math.pi * math.pow(r, 2)
    print('Área: ', area)
else:
    print('*** Triángulo *** ')
    b = int(input('Base: '))
    h = int(input('Altura: '))
    area = b * h / 2
    print('Área: ', area)