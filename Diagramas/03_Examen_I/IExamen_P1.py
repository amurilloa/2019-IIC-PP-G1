a = 10
b = 8
c = 5

r = (b > a) or ((b > c) and (a < c))
print('(b > a)||((b > c)&&(a < c)) : ', r)

r = (b < a) and (b > c) and (a < c)
print('(b < a)&&(b > c)&&(a < c) : ', r)

r = (a < c) and ((a + c) < b)
print('(a < c) and ((a + c) < b) : ', r)

r = not ((b > a) and (b > c)) or (a > c)
print('!((b > a)&&(b > c))||(a > c) : ', r)

r = ((b - c) <= a) and not (b < c) or (a < c)
print('((b - c) <= a)&& !(b < c)||(a < c) : ', r)

