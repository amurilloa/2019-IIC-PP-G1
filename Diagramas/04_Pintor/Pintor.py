import math


# areaRectangulo
# areaCirculo

# Medir la pared
# 4x8=32

# ¿ Tiene ventanas rectagulares ?
# 2x4=8
# ¿ Tiene ventanas circulares ?
# Diametro: 2, área: 3.14

# Total paredes: 32 m²
# Total de ventanas: 11.14 m²
# Total a pintar: 20.86 m²
# Costo por galón aplicado: $20 (1 Galón por 10 m²)
# Presupuesto: 3 gal x $20= $60


# Tarea 1: Pasar todos los diagramas secuenciales y condicionales
# a funciones, llamarlos de un menú.

# Cuanto mide la pared
# Alto pared:
# Ancho pared:

# ¿ Tiene ventana rectagular ?
# SI
# Ancho Ventana
# Alto Ventana

# NO


# ¿ Tiene ventana circular ?
# SI
# Diametro de la ventana
# NO

# Cotización
# Total paredes: 32 m²
# Total de ventanas: 11.14 m²
# Total a pintar: 20.86 m²
# Costo por galón aplicado: $20 (1 Galón por 10 m²)
# Presupuesto: 3 gal x $20= $60


# Aplicación ---> Interfaz/Interacción con el usuario: Entradas y Salidas de Datos
#            ---> Lógica de la aplicación: Procedimientos, métodos o funciones que resuelven el problema


# Lógica
def areaRect(ancho, alto):
    """
    Area de un Rectangulo
    :param ancho: Base/Ancho de la superficie
    :param alto: Alto de la superficio
    :return: área redondeada al entero siguiente
    """
    area = ancho * alto
    return round(area + 0.5)


def areaCirc(diametro):
    r = diametro / 2
    area = math.pi * r ** 2
    return round(area + 0.5)


menuV = ('¿Desea agregar una ventana?\n'
         '  1.SI, Ventana Rectangular\n'
         '  2.SI, Ventana Circular\n'
         '  3.NO\n'
         'Seleccione una opción: ')

prePared = ('¿Desea agregar una pared?\n'
            '1.SI\n'
            '2.NO\n'
            'Seleccione una opción: ')
# Interfaz
print('Pintor - UTN v0.1')
areaP = 0
areaV = 0
while True:
    op = int(input(prePared))
    if op == 1:
        base = float(input('Ancho(metros): '))
        altura = float(input('Alto(metros): '))
        areaP += areaRect(base, altura)
        while True:
            op = int(input(menuV))
            if op == 1:
                print('¿Cuánto mide la Ventana?')
                base = float(input('Ancho(metros): '))
                altura = float(input('Alto(metros): '))
                areaV = areaV + areaRect(base, altura)
            elif op == 2:
                print('¿Cuánto mide la Ventana?')
                diametro = float(input('Diámetro(metros): '))
                temp = areaCirc(diametro)
                print('Area de Ventana Circular:', temp)
                areaV = areaV + temp
            elif op == 3:
                break
            else:
                print('Opción Inválida!!')
    elif op == 2:
        break
    else:
        print('Opción Inválida!!')
if areaV > areaP:
    print('El área de las ventanas no puede ser mayor al área de la pared')
else:

    COS_X_GAL = 20  # $20 el costo del galón
    M2_X_GAL = 10  # 1galón alcanza para 10m²
    areaPin = areaP - areaV
    gal = round(areaPin / M2_X_GAL + 0.5)
    cos = gal * COS_X_GAL

    print(' Total área paredes:', areaP, 'm²')
    print('Total área ventanas:', areaV, 'm²')
    print('Total área a pintar:', areaPin, 'm²')
    print('Costo por galón aplicado: $20(1 Galón por 10 m²')
    print('Presupuesto:', gal, 'gal x $', COS_X_GAL, '=$', cos)
