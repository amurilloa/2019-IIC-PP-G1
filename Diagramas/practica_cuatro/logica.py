# Maraton
# inscripcion, cedula, nombre, fecha nacimiento, categoria
numIns = 1000
atletas = {}
categorias = ['Maratón', 'Media Maratón', '10k', '5k']
razas = ['Pastor Alemán', 'Boxer', 'Pitbull', 'Husky', 'Otro']


def registrar(atleta):
    # ced:(ced, nom, fecha, cat, num)
    ced = atleta[0]
    atletas[ced] = atleta


def numSig():
    global numIns
    numIns = numIns + 1
    return numIns


def pregunta(preg):
    preg += '\nDigite SI u otro para NO: '
    while True:
        res = input(preg).lower()
        if len(res) > 0:
            res = res[0]
            if res == 's' or res == 'y':
                return True
            elif res == 'n':
                return False
        print('Favor digitar los solicitado. ')


def leerInt(mensaje, max=-1):
    """
    Para que sirve o el objetivo del método
    :param mensaje: que hace en el método el mensaje ???
    :param max: que hace el parámetro max ? para que sirve ??
    :return: Que retorna el método ?
    """
    while True:
        try:
            res = int(input(mensaje))
            if (res >= 1 and res <= max) or max == -1:
                return res
            else:
                print('\nOpción inválida, ', end='')
        except ValueError:
            print('\nFavor intente nuevamente, ', end='')


def impLista(lista):
    txt = ''
    cont = 1  # Es para mostrar al usuario, no podemos empezarlo en 0
    for x in lista:
        txt += '{0}. {1}\n'.format(cont, x)
        cont += 1
    return txt


def leerTxt(mensaje, req=True, ant=''):
    while True:
        if len(ant) > 0:
            mensaje += '({0})'.format(ant)
        txt = input(mensaje).strip()  # elimina espacios en blanco al principio o final del texto
        if len(txt) > 0 and req:
            return txt
        elif not req and len(ant) > 0:
            return ant
        elif not req:
            return txt
        else:
            print('Campo requerido. ', end='')


def impAtletas():
    txt = '\nLista de Atletas Inscritos\n\n'
    con = 1
    for key in atletas:
        obj = atletas[key]
        txt += '{0}. #{1} {2}({3})\n'.format(con, obj[4], obj[1], obj[3])
        con += 1
    return txt


def buscar(filtro):
    txt = '\nLista de Atletas Inscritos\n\n'
    con = 1
    for key in atletas:
        obj = atletas[key]
        if filtro in obj:
            txt += '{0}. #{1} {2}({3})\n'.format(con, obj[4], obj[1], obj[3])
            con += 1
    return txt


registrar([206520946, 'Lineth Matamoros', '17/11/1988', categorias[2], numSig()])
registrar([206520947, 'Josué Matamoros', '17/11/1988', categorias[3], numSig()])
registrar([206520948, 'Alberto Matamoros', '17/11/1988', categorias[3], numSig()])

print(impAtletas())
