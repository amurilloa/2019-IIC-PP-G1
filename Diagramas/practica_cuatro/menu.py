import practica_cuatro.logica as log

menuP = ('\nPráctica 4\n'
         '1. Registrar objeto\n'
         '2. Modificar un objeto\n'
         '3. Imprimir objetos (todos)\n'
         '4. Buscar objeto\n'
         '5. Salir\n'
         'Favor seleccione una opción: ')

menuB = ('\nPráctica 4 - Buscar\n'
         '1. Cédula\n'
         '2. Número de Inscripción\n'
         '3. Categoría\n'
         '4. Volver\n'
         'Favor seleccione una opción: ')

while True:
    op = log.leerInt(menuP, 5)
    if op == 1:
        ced = log.leerInt('Cédula: ')
        nom = log.leerTxt('Nombre: ')
        fec = log.leerTxt('Fecha Nacimiento: ')
        print(log.impLista(log.categorias))
        pos = log.leerInt('Seleccione una opción', len(log.categorias))
        cat = log.categorias[pos - 1]
        num = log.numSig()
        atleta = [ced, nom, fec, cat, num]
        log.registrar(atleta)
    elif op == 2:
        ced = log.leerInt('Cédula a modificar: ')
        if ced in log.atletas:
            atletaVie = log.atletas[ced]
            nom = log.leerTxt('Nombre: ', False, atletaVie[1])
            fec = log.leerTxt('Fecha Nacimiento: ', False, atletaVie[2])
            print(log.impLista(log.categorias))
            pos = log.leerInt('Cat. Ant: {0}\nSeleccione una opción: '.format(atletaVie[3]), len(log.categorias))
            cat = log.categorias[pos - 1]
            num = atletaVie[4]
            atleta = [ced, nom, fec, cat, num]
            log.registrar(atleta)
        else:
            print('Atleta no registrado!')
    elif op == 3:
        print(log.impAtletas())
    elif op == 4:
        while True:
            op = log.leerInt(menuB, 4)
            if op == 1:
                filtro = log.leerInt('Cédula: ')
            elif op == 2:
                filtro = log.leerInt('Número de inscripción: ')
            elif op == 3:
                print(log.impLista(log.categorias))
                pos = log.leerInt('Seleccione una opción', len(log.categorias))
                filtro = log.categorias[pos - 1]
            else:
                break
            print(log.buscar(filtro))
    else:
        print('Gracias por utilizar la aplicación')
        break
