productos = {'Huevos': 0, 'Leche': 4, 'Carne Res': 0, 'Cerdo': 2, 'Atunes': 10}

# Cuales se acabaron
def agotados():
    txt = '\nLista de Productos Agotados\n'
    con = 1
    for key in productos:
        if productos[key] == 0:
            txt += '{0}. {1}\n'.format(con, key)
            con += 1
    return txt

# Cuanto hay de un producto
def buscarKey(prod):
    for x in productos.keys():
        if x.lower()==prod.lower():
            return x
    return prod

def buscar(pro):
    pro = buscarKey(pro)
    if pro in productos:
        can = productos[pro]
        msj = '\nCantidad de {0}: {1}\n'.format(pro, can)
        return (can, msj)
    else:
        return (-1, 'Producto no registrado!!')


# Comprar una cantidad de un producto
def comprar(prod, cant):
    prod = buscarKey(prod)
    if prod in productos:
        actual = productos[prod]
        actual += cant
        productos[prod] = actual
        return 'Producto actializado'
    else:
        return 'Producto no existe'