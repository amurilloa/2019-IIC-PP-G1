import diccionarios.logica as log
import diccionarios.util as util

menuP = ('Ejercicio 3 - Refrigerador Inteligente\n'
         '1. Agotados\n'
         '2. Consulta por producto\n'
         '3. Compra de productos\n'
         '4. Salir\n'
         'Favor seleccione una opción: ')

while True:
    op = util.leerInt(menuP, 4)
    if op == 1:
        print(log.agotados())
    elif op == 2:
        filtro = util.leerTxt('Producto: ')
        can, mes = log.buscar(filtro)
        print(mes)
    elif op == 3:
        filtro = util.leerTxt('Producto: ')
        can, mes = log.buscar(filtro)
        if can > -1:
            unidades = util.leerInt('Cantidad a comprar: ')
            log.comprar(filtro, unidades)
        else:
            print('No puede comprar este artículo')
    else:
        print('Gracias por utilizar nuestra aplicación')
        break

dic = {2012021: [2012021, 'Nombre 1', 'corre@ads.com'],
       2012022: [2012022, 'Nombre 2', 'corre@ads.com']}


def medicos():
    cont = 1
    txt = ''
    for x in dic:
        medico = dic[x]
        txt += '{0}. {1} - {2}\n'.format(cont, medico[0], medico[1])
        cont += 1
    return txt


print(dic)
print(medicos())

pos = util.leerInt('Seleccione un médico', len(dic))

lista = list(dic.keys())
med = lista[pos-1]
print(med)
print(dic[med])