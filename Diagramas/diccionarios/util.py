def pregunta(preg):
    preg += '\nDigite SI u otro para NO: '
    while True:
        res = input(preg).lower()
        if len(res) > 0:
            res = res[0]
            if res == 's' or res == 'y':
                return True
            elif res == 'n':
                return False
        print('Favor digitar los solicitado. ')

def leerInt(mensaje, max=-1):
    """
    Para que sirve o el objetivo del método
    :param mensaje: que hace en el método el mensaje ???
    :param max: que hace el parámetro max ? para que sirve ??
    :return: Que retorna el método ?
    """
    while True:
        try:
            res = int(input(mensaje))
            if (res >= 1 and res <= max) or max == -1:
                return res
            else:
                print('\nOpción inválida, ', end='')
        except ValueError:
            print('\nFavor intente nuevamente, ', end='')

def impLista(lista):
    txt = ''
    cont = 1  # Es para mostrar al usuario, no podemos empezarlo en 0
    for x in lista:
        txt += '{0}. {1}\n'.format(cont, x)
        cont += 1
    return txt

def leerTxt(mensaje, req=True, ant=''):
    while True:
        if len(ant) > 0:
            mensaje += '({0})'.format(ant)
        txt = input(mensaje).strip()  # elimina espacios en blanco al principio o final del texto
        if len(txt) > 0 and req:
            return txt
        elif not req and len(ant) > 0:
            return ant
        elif not req:
            return txt
        else:
            print('Campo requerido. ', end='')
